import sys, os, numpy, glob,  cPickle, shutil, ntpath, csv
import matplotlib.pyplot as plt
import mlpy

def listOfFeatures2Matrix(features):
	'''
	listOfFeatures2Matrix(features)
	
	This function takes a list of feature matrices as argument and returns a single concatenated feature matrix and the respective class labels.

	ARGUMENTS:
		- features:		a list of feature matrices

	RETURNS:
		- X:			a concatenated matrix of features
		- Y:			a vector of class indeces	
	'''

	X = numpy.array([])
	Y = numpy.array([])
	for i,f in enumerate(features):
		if i==0:
			X = f
			Y = i * numpy.ones((len(f), 1))
		else:
			X = numpy.vstack((X, f))
			Y = numpy.append(Y, i * numpy.ones((len(f), 1)))
	return (X, Y)


def trainSVM(features, Cparam):
	'''
	Train a multi-class probabilitistic SVM classifier.
	Note: 	This function is simply a wrapper to the mlpy-LibSVM functionality for SVM training
		See function trainSVM_feature() to use a wrapper on both the feature extraction and the SVM training (and parameter tuning) processes.
	ARGUMENTS:
		- features: 		a list ([numOfClasses x 1]) whose elements containt numpy matrices of features.
					each matrix features[i] of class i is [numOfSamples x numOfDimensions] 
		- Cparam:		SVM parameter C (cost of constraints violation)
	RETURNS:
		- svm:			the trained SVM variable

	NOTE:	
		This function trains a linear-kernel SVM for a given C value. For a different kernel, other types of parameters should be provided.
		For example, gamma for a polynomial, rbf or sigmoid kernel. Furthermore, Nu should be provided for a nu_SVM classifier.
		See MLPY documentation for more details (http://mlpy.sourceforge.net/docs/3.4/svm.html)
	'''
	[X, Y] = listOfFeatures2Matrix(features)	
	svm = mlpy.LibSvm(svm_type='c_svc', kernel_type='linear', eps=0.0000001, C = Cparam, probability=True)
	svm.learn(X, Y)	
	return svm

def getSec(s):
	'''
	Convert time string of the format : HH:MM:SS.D to seconds
	'''
	l = s.split(':')
	return float(l[0]) * 3600 + float(l[1]) * 60 + float(l[2])

def getSecVisual(s):
	'''
	Convert time string of the format : HH:MM:SS.D to seconds
	'''
	l = s.split(':')
	return float(l[0]) * 3600 + float(l[1]) * 60 + float(l[2]) + float(l[3])/100.0


def readAudioCSV(csvFile, PLOT = False):
	'''
	Read the audio CSV file of the format: <time string>, <DOA>, <Energy>
	ARGUMENTS:
		- csvFile:	the path of the input CSV file
		- PLOT (opt):	True if DOA and Energy need to be plotted
	RETURNS:
		- audioTime:	a numpy array of the timestamps in seconds
		- audioDOA:	numpy DOA array
		- audioE:	numpy array of energy values
	'''
	countLines = 0
	audioE = []
	audioDOA = []
	audioTime = []
	with open(csvFile, 'rb') as csvF:
		csvF = csv.reader(csvF, delimiter=',', quotechar='|')
		for row in csvF:
			audioTime.append(getSec(row[0]))
			audioDOA.append(float(row[1]))
			audioE.append(float(row[2]))
			countLines += 1

	audioTime = numpy.array(audioTime)
	audioDOA = numpy.array(audioDOA)
	audioE = numpy.array(audioE)

	if PLOT:
		plt.subplot(2,1,1); plt.plot(audioDOA); plt.ylabel('DOA');
		plt.subplot(2,1,2); plt.plot(audioE);  plt.ylabel('Energy'); plt.show()

	return audioTime, audioDOA, audioE
	

def readVisualCSV(csvFile, PLOT = False):
	'''
	Read the audio CSV file of the format: 
	ARGUMENTS:
		- csvFile:	the path of the input CSV file
		- PLOT (opt):	True if data are to be plotted
	RETURNS:

	'''
	countLines = 0

	visualTime = []
	posX = []
	posY = []
	posZ = []
	lbps = []
	frameID = []
	with open(csvFile, 'rb') as csvF:
		csvF = csv.reader(csvF, delimiter=',', quotechar='|')
		for row in csvF:
			if countLines > 0:
				visualTime.append(getSecVisual(row[0]))		# time stamp		
				frameID.append(int(row[1]))
				# skip Rect_x, Rect_y, Rect_W, Rect_H
				posX.append(float(row[6]))
				posY.append(float(row[7]))
				posZ.append(float(row[8]))
				# skip orientation
				#tempLbp = [float(l) for l in row[10::]]
				tempLbp = [float(l) for l in row[-3::]]
				print tempLbp
				lbps.append(tempLbp)

				#audioE.append(float(row[2]))
			countLines += 1
	visualTime = numpy.array(visualTime)
	visualTime = visualTime - visualTime[0]
	posX = numpy.array(posX)
	posY = numpy.array(posY)
	posZ = numpy.array(posZ)
	lbps = numpy.matrix(lbps)
	frameID = numpy.array(frameID)

	#if PLOT:
	#	plt.subplot(2,1,1); plt.plot(audioDOA); plt.ylabel('DOA');
	#	plt.subplot(2,1,2); plt.plot(audioE);  plt.ylabel('Energy'); plt.show()

	return visualTime, frameID, posX, posY, posZ, lbps


def main(argv):
	if argv[1] == "-lipMovementDetectionOnline":				
		audioTime, audioDOA, audioE = readAudioCSV(argv[2])
		visualTime, frameID, posX, posY, posZ, lbps = readVisualCSV(argv[3])

		SVMtrained = False

		# merge audio-visual

		plt.subplot(4,1,1)
		for i in range(posX.shape[0]):
			plt.plot(posX[i], posZ[i], '*')
		plt.axis('equal')
		plt.subplot(4,1,2);
		plt.plot(audioTime,audioE)
		eT = 0.2 * audioE.mean()
		speakingLBPs = []
		nonSpeakingLBPs = []
						

		CM = numpy.zeros((2,2))


		for i, aT in enumerate(audioTime):
			iVisual = numpy.argmin(numpy.abs(aT - visualTime))			# find nearest visual timestamp
			iVisual = numpy.nonzero(frameID == frameID[iVisual])[0]			# find all visual indices with the same frame ID 
			vDOAS = numpy.degrees(numpy.arctan(posX[iVisual] / posZ[iVisual]))	# current video angles (only for the current frame)
			plt.subplot(4,1,3)
			for ii, iV in enumerate(iVisual):
				plt.plot( visualTime[iV] , vDOAS[ii], 'k*')
			plt.plot( aT , audioDOA[i], 'r*')

			curLBPs = lbps[iVisual]
			
#			print vDOAS, audioDOA[i]

			if audioE[i] > eT:								# if is talking	
				currentDoa = audioDOA[i]						# get current video doa:
				closestVDoaIndex = numpy.argmin( numpy.abs(currentDoa - vDOAS) )			
				if numpy.abs(currentDoa - vDOAS[closestVDoaIndex]) > 15:
					nonSpeakingLBPs.append(numpy.squeeze(numpy.asarray(curLBPs[closestVDoaIndex, :])))
#					print "No: {0:.2f}\t".format(vDOAS[closestVDoaIndex]), 
					if SVMtrained:
						svmResult = SVM.pred(numpy.squeeze(numpy.asarray(curLBPs[closestVDoaIndex, :])));
						print "NON: {0:.2f}".format(svmResult)
						CM[1,svmResult] += 1
				else:
					speakingLBPs.append(numpy.squeeze(numpy.asarray(curLBPs[closestVDoaIndex, :])))
#					print "Yes: {0:.2f}\t".format(vDOAS[closestVDoaIndex]), 
					if SVMtrained:
						svmResult = SVM.pred(numpy.squeeze(numpy.asarray(curLBPs[closestVDoaIndex, :])));
						print "SPEAK: {0:.2f}".format(svmResult)
						CM[0,svmResult] += 1

				for iv in range(iVisual.shape[0]):
					if iv != closestVDoaIndex:
#						print "No: {0:.2f}\t".format(vDOAS[iv]),
						nonSpeakingLBPs.append(numpy.squeeze(numpy.asarray(curLBPs[iv, :])))
					if SVMtrained:
						svmResult = SVM.pred(numpy.squeeze(numpy.asarray(curLBPs[iv, :])))
						print "NON: {0:.2f}".format(svmResult)
						CM[1,svmResult] += 1

#			print
#			print
			if len(speakingLBPs) > 15 and len(nonSpeakingLBPs)>15:
				speakingLBPsNUMPY = numpy.matrix(speakingLBPs)
				nonSpeakingLBPsNUMPY = numpy.matrix(nonSpeakingLBPs)
				SVM = trainSVM([speakingLBPsNUMPY, nonSpeakingLBPsNUMPY], 1.0)
				SVMtrained = True
			plt.subplot(4,1,4)
			AC = (CM[0,0]+CM[1,1]) / CM.sum()
			Pre = (CM[0,0]) / (CM[0,0] + CM[1,0])
			Rec = (CM[0,0]) / (CM[0,0] + CM[0,1])
			plt.plot(aT, AC, 'kx');
			plt.plot(aT, Rec, 'gx');
			plt.plot(aT, Pre, 'rx');
			plt.draw()
			plt.show(block = False)
		print len(speakingLBPs)
		print len(nonSpeakingLBPs)
		print CM
		plt.show()

#		print posX
#		plt.subplot(2,1,1); 
#		plt.plot(posX)
		#plt.plot(numpy.degrees(numpy.arctan(posZ / posX)))
#		plt.subplot(2,1,2); plt.plot(audioDOA)
#		plt.show()
	

if __name__ == '__main__':
	main(sys.argv)
